# stellar7.js

A clone of the stellar7 game (commodore64 version) written in pure js and canvas (without any use of webGL). I wrote a basic 3d engine (software renderer) from scratch and then made the game on top of it.

## How to play

To play the game You just need to open the index.html file in your browser. Victory condition is killing all your enemies (becareful, they cen shoot at you!). Here is the keybindings list:

* Arrow keys - moving and turning your vehicle.
* F key - shooting.