function Projectile(scale,meshes,rot,pos,owner,camera) {
    Object.assign(this,new Mesh([new V3(-1,1,-2),new V3(1,1,-2),new V3(-1,-1,-2),new V3(1,-1,-2),new V3(0,0,2)],[[0,1],[1,3],[2,3],[2,0],[0,4],[1,4],[2,4],[3,4]],scale,"projectile"))
    let min = new V3(0,0,0)
    let max = new V3(0,0,0)
    for(let vert of this.verts) {
        for(let coord of ["x","y","z"]) {
            if(vert[coord] < min[coord]) min[coord] = vert[coord]
            if(vert[coord] > max[coord]) max[coord] = vert[coord] 
        }
    }
    let aV = min.clone().add(max).multiply(0.5)
    for(let vert of this.verts) vert.sub(aV)
    max.sub(aV).multiply(scale)
    min.sub(aV).multiply(scale)
	let index = meshes.length
    meshes.push(this)
    this.rot = rot
    this.pos = pos

	this.worker = () => {
        this.pos.add((new V3(0,0,scale*2.5)).rotate(new V3(this.rot.x,this.rot.y,0)))
        let nMax = max.clone().rotate(this.rot).add(this.pos)
        let nMin = min.clone().rotate(this.rot).add(this.pos)
        for(let mesh of meshes) {
            if(!mesh || mesh.name == "projectile" || mesh.name == owner) continue;
            if(mesh.name == "cube" && mesh.pos.clone().sub(this.pos).length2() <= 15) this.destroy();
            if(mesh.name == "plane" && mesh.pos.clone().sub(this.pos).length2() <= 20) mesh.destroy(), camera.stats.score += 30, meshes[index] = undefined;
            if(mesh.name == "tank" && mesh.pos.clone().sub(this.pos).length2() <= 10) mesh.destroy(), camera.stats.score += 20, meshes[index] = undefined;
        }
        if(owner != "camera" && camera.pos.clone().sub(this.pos).length2() <= 5) camera.colors(), camera.stats.hg += 0.05, meshes[index] = undefined
        if(nMin.y < 0) this.destroy();
        if(this.pos.length() > 500) meshes[index] = undefined;
        if(meshes[index]) window.requestAnimationFrame(this.worker)
    }

    this.worker()

    this.destroy = () => {
        meshes[index] = undefined;

    }

}