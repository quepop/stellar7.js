function init() {
    let canvas = document.getElementById("game")
    let ctx = canvas.getContext("2d")
    let meshes = []
    let stats = {hg: 0,eg: 0, score: 0, black: false}
    let camera = new Camera(stats,ctx,P2((8/320)*1280,(32/200)*800),P2((256/320)*1280,(192/200)*800),meshes,new V3(0,0,0),new V3(0,0,0));
    let plane = new Plane(9,meshes,camera)
	let tank = new Tank(3.5,meshes,camera)
    let keys = {"38": false, "40": false, "39": false, "37": false, "70": false}
    let fired = Date.now() + 700;
    


    for(let pos of [[1,1],[-1,1],[-1,-1],[1,-1]]){
        let cube = new Mesh([new V3(-1,-1,-1),new V3(1,-1,-1),new V3(1,1,-1), new V3(-1,1,-1), new V3(-1,-1,1), new V3(1,-1,1), new V3(1,1,1), new V3(-1,1,1)],[[0,1],[1,2],[2,3],[3,0],[4,5],[5,6],[6,7],[7,4],[0,4],[1,5],[2,6],[3,7]],10,"cube")
        cube.pos.x = pos[0]*180
        cube.pos.z = pos[1]*180
        cube.pos.y = 10
        meshes.push(cube)
    }
    plane.pos.z += 290
    tank.pos.z += 160
    camera.pos.y  += 8

    function lost() {
        let current = 0;
        camera.colors();
        let tn = setInterval(function(){
            if(++current > 15) clearInterval(tn), stats.black = true;
            camera.colors()
        },240)
    }

    function secondW () {
        camera.updateFrame();
        
        ctx.fillStyle = "rgb(87,87,83)"
        ctx.fillRect(0,0,(8/320)*1280,800);
        ctx.fillRect(0,0,1280,(32/200)*800);
        ctx.fillRect(0,(192/200)*800,1280,(8/200)*800);
        ctx.fillRect(1280*(256/320),0,1280,800);

        ctx.fillStyle = "rgb(251,251,139)"
        ctx.fillRect((3/320)*1280,(30/200)*800,(258/320)*1280,(2/200)*800);
        ctx.fillRect((3/320)*1280,(30/200)*800,(4/320)*1280,(164/200)*800);
        ctx.fillRect((3/320)*1280,(192/200)*800,(258/320)*1280,(2/200)*800);
        ctx.fillRect((257/320)*1280,(30/200)*800,(4/320)*1280,(164/200)*800);

        ctx.fillRect((269/320)*1280,0,(38/320)*1280,(2/200)*800);
        ctx.fillRect((269/320)*1280,0,(3/320)*1280,(32/200)*800);
        ctx.fillRect((269/320)*1280,(30/200)*800,(38/320)*1280,(2/200)*800);
        ctx.fillRect((304/320)*1280,0,(3/320)*1280,(32/200)*800);

        ctx.fillStyle = "rgb(79,68,216)"
        ctx.fillRect((272/320)*1280,(2/200)*800,(32/320)*1280,(28/200)*800);

        ctx.fillStyle = "rgb(251,251,139)"
        ctx.fillRect((287/320)*1280,(15/200)*800,(1/320)*1280,(1/200)*800);


        for(let mesh of meshes) {
            if(!mesh) continue
            let V = mesh.pos.clone().sub(camera.pos).rotate(new V3(0,-camera.rot.y,0))
            if(mesh.name == "projectile" || Math.abs(V.x/20) > 17 || Math.abs(V.z/20) > 15) continue
            ctx.fillRect((287/320)*1280+(V.x/20)*(1/320)*1280,(15/200)*800-(V.z/20)*(1/200)*800,(1/320)*1280,(1/200)*800);
        }

        ctx.fillStyle = "black"
        ctx.fillRect((272/320)*1280,(40/200)*800,(8/320)*1280,(152/200)*800);
        ctx.fillRect((296/320)*1280,(40/200)*800,(8/320)*1280,(152/200)*800);

        ctx.fillStyle = stats.hg >= 0.80 ? "rgb(147,58,76)" : "rgb(79,68,216)"
        ctx.fillRect((272/320)*1280,((40+(192-40)*stats.hg)/200)*800,(8/320)*1280,(152*(1-stats.hg)/200)*800);
        ctx.fillStyle = stats.eg >= 0.80 ? "rgb(147,58,76)" : "rgb(210,125,237)"
        ctx.fillRect((296/320)*1280,((40+(192-40)*stats.eg)/200)*800,(8/320)*1280,(152*(1-stats.eg)/200)*800);

        for(let i = 0; i < 38; ++i) {
            ctx.fillStyle = "white"
            ctx.fillRect((272/320)*1280,((40+(4*i))/200)*800,(8/320)*1280,(1/200)*800);
            ctx.fillRect((296/320)*1280,((40+(4*i))/200)*800,(8/320)*1280,(1/200)*800);
        }

        ctx.fillStyle = "rgb(79,68,216)"
        ctx.fillRect((8/320)*1280,0,(56/320)*1280,(24/200)*800);

        ctx.fillStyle = "white"
        ctx.fillRect((10/320)*1280,0,(52/320)*1280,(2/200)*800);
        ctx.fillRect((9/320)*1280,(1/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((60/320)*1280,(1/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((10/320)*1280,(22/200)*800,(52/320)*1280,(2/200)*800);
        ctx.fillRect((9/320)*1280,(21/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((60/320)*1280,(21/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((8/320)*1280,(2/200)*800,(3/320)*1280,(20/200)*800);
        ctx.fillRect((61/320)*1280,(2/200)*800,(3/320)*1280,(20/200)*800);

        ctx.font = (10/200)*800 + 'px "asd"';
        ctx.textBaseline = 'top';
        ctx.fillText  ('SCORE', (16/320)*1280, (3/200)*800);

        let scoreString = "0000" + stats.score.toString()
        ctx.fillText  (scoreString.substr(scoreString.length-5, scoreString.length), (16/320)*1280, (10.5/200)*800);
        window.requestAnimationFrame(secondW);
    }

    function worker() {
        if(stats.hg >= 1) {
            stats.hg = 1;
            lost();
            secondW();
            meshes = []
            return
        }

        if(keys["38"]) {
            let coll = camera.pos.clone().add((new V3(0,0,0.5)).rotate(new V3(0,camera.rot.y,0)))
            let cann = true;
            for(let mesh of meshes) {
                if(!mesh || mesh.name == "projectile") continue;
                if(mesh.name == "cube" && mesh.pos.clone().sub(coll).length2() <= 15) cann = false;
                if(mesh.name == "plane" && mesh.pos.clone().sub(coll).length2() <= 15) cann = false;
                if(mesh.name == "tank" && mesh.pos.clone().sub(coll).length2() <= 15) cann = false;
            }
            if(cann) camera.pos.add((new V3(0,0,0.5)).rotate(new V3(0,camera.rot.y,0)))
        }
        if(keys["40"]) camera.pos.add((new V3(0,0,0.5)).rotate(new V3(0,camera.rot.y,0)).multiply(-1))
        if(keys["39"]) camera.rot.y += 0.03
        if(keys["37"]) camera.rot.y -= 0.03
        if(keys["70"]) {
			if(Date.now()-fired > 700) {
			let p = new Projectile(1.2,meshes,camera.rot.clone(),camera.pos.clone(),"camera",camera)
			fired = Date.now();
        }}
        camera.updateFrame();

        ctx.fillStyle = "rgb(87,87,83)"
        ctx.fillRect(0,0,(8/320)*1280,800);
        ctx.fillRect(0,0,1280,(32/200)*800);
        ctx.fillRect(0,(192/200)*800,1280,(8/200)*800);
        ctx.fillRect(1280*(256/320),0,1280,800);

        ctx.fillStyle = "rgb(251,251,139)"
        ctx.fillRect((3/320)*1280,(30/200)*800,(258/320)*1280,(2/200)*800);
        ctx.fillRect((3/320)*1280,(30/200)*800,(4/320)*1280,(164/200)*800);
        ctx.fillRect((3/320)*1280,(192/200)*800,(258/320)*1280,(2/200)*800);
        ctx.fillRect((257/320)*1280,(30/200)*800,(4/320)*1280,(164/200)*800);

        ctx.fillRect((269/320)*1280,0,(38/320)*1280,(2/200)*800);
        ctx.fillRect((269/320)*1280,0,(3/320)*1280,(32/200)*800);
        ctx.fillRect((269/320)*1280,(30/200)*800,(38/320)*1280,(2/200)*800);
        ctx.fillRect((304/320)*1280,0,(3/320)*1280,(32/200)*800);

        ctx.fillStyle = "rgb(79,68,216)"
        ctx.fillRect((272/320)*1280,(2/200)*800,(32/320)*1280,(28/200)*800);

        ctx.fillStyle = "rgb(251,251,139)"
        ctx.fillRect((287/320)*1280,(15/200)*800,(1/320)*1280,(1/200)*800);


        for(let mesh of meshes) {
            if(!mesh) continue
            let V = mesh.pos.clone().sub(camera.pos).rotate(new V3(0,-camera.rot.y,0))
            if(mesh.name == "projectile" || Math.abs(V.x/20) > 17 || Math.abs(V.z/20) > 15) continue
            ctx.fillRect((287/320)*1280+(V.x/20)*(1/320)*1280,(15/200)*800-(V.z/20)*(1/200)*800,(1/320)*1280,(1/200)*800);
        }

        ctx.fillStyle = "black"
        ctx.fillRect((272/320)*1280,(40/200)*800,(8/320)*1280,(152/200)*800);
        ctx.fillRect((296/320)*1280,(40/200)*800,(8/320)*1280,(152/200)*800);

        ctx.fillStyle = stats.hg >= 0.80 ? "rgb(147,58,76)" : "rgb(79,68,216)"
        ctx.fillRect((272/320)*1280,((40+(192-40)*stats.hg)/200)*800,(8/320)*1280,(152*(1-stats.hg)/200)*800);
        ctx.fillStyle = stats.eg >= 0.80 ? "rgb(147,58,76)" : "rgb(210,125,237)"
        ctx.fillRect((296/320)*1280,((40+(192-40)*stats.eg)/200)*800,(8/320)*1280,(152*(1-stats.eg)/200)*800);

        for(let i = 0; i < 38; ++i) {
            ctx.fillStyle = "white"
            ctx.fillRect((272/320)*1280,((40+(4*i))/200)*800,(8/320)*1280,(1/200)*800);
            ctx.fillRect((296/320)*1280,((40+(4*i))/200)*800,(8/320)*1280,(1/200)*800);
        }

        ctx.fillStyle = "rgb(79,68,216)"
        ctx.fillRect((8/320)*1280,0,(56/320)*1280,(24/200)*800);

        ctx.fillStyle = "white"
        ctx.fillRect((10/320)*1280,0,(52/320)*1280,(2/200)*800);
        ctx.fillRect((9/320)*1280,(1/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((60/320)*1280,(1/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((10/320)*1280,(22/200)*800,(52/320)*1280,(2/200)*800);
        ctx.fillRect((9/320)*1280,(21/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((60/320)*1280,(21/200)*800,(3/320)*1280,(2/200)*800);
        ctx.fillRect((8/320)*1280,(2/200)*800,(3/320)*1280,(20/200)*800);
        ctx.fillRect((61/320)*1280,(2/200)*800,(3/320)*1280,(20/200)*800);

        ctx.font = (10/200)*800 + 'px "asd"';
        ctx.textBaseline = 'top';
        ctx.fillText  ('SCORE', (16/320)*1280, (3/200)*800);

        let scoreString = "0000" + stats.score.toString()
        ctx.fillText  (scoreString.substr(scoreString.length-5, scoreString.length), (16/320)*1280, (10.5/200)*800);

        plane.worker();
        tank.worker();

        let win = true;
        for(let mesh of meshes) if(mesh && mesh.name != "projectile" && mesh.name != "cube") win = false;
        if(win) ctx.fillText("WIN!", (120/320)*1280, (14/200)*800);

        requestAnimationFrame(worker)

    }

    worker();

    window.onkeydown = (e) => { if(keys[e.keyCode] != undefined) keys[e.keyCode] = true}
    window.onkeyup = (e) => { if(keys[e.keyCode] != undefined) keys[e.keyCode] = false}

}

window.onload = init

