
function Tank(scale,meshes,camera) {
	Object.assign(this,new Mesh([new V3(0,0,0),new V3(1.000000, -1.000000, -1.000000),new V3( 1.000000, -1.000000, 3.764010),new V3( -1.000000, -1.000000, 3.764010),new V3( -1.000000, -1.000000, -1.000000),new V3( -0.00001, 1.000000, 0.00001),new V3( -1.000000, -1.426300, -1.116453),new V3( 1.000000, -1.387244, 6.835930),new V3( -1.000000, -1.387244, 6.835930),new V3( 1.000000, -1.426300, -1.116453),new V3( 0.333333, -1.426300, -1.116453),new V3( -0.333333, -1.426300, -1.116453),new V3( -0.333333, -1.387244, 6.835930),new V3( 0.333333, -1.387244, 6.835930),new V3( 1.000000, -1.426300, 6.438311),new V3( 0.333333, -1.426300, 6.438311),new V3( -1.000000, -1.426300, 6.438311),new V3( -0.333333, -1.426300, 6.438311),new V3( 0.338133, 0.043822, 1.860779),new V3( 0.00001, 0.043822, 4.789137),new V3( -0.338133, 0.043822, 1.860779),new V3( -0.00001, 0.330493, 1.301946)], [[16, 17],[1, 2],[2 ,3],[3 ,4],[4 ,1],[17, 11],[11, 6],[6 ,16],[1 ,5],[5 ,2],[2 ,1],[3 ,5],[5 ,4],[5 ,1],[1 ,4],[4 ,5],[15, 14],[14, 9],[9 ,10],[10, 15],[14, 15],[17, 16],[18, 19],[19, 20],[18, 21],[21, 19],[19, 18],[21, 20],[21, 18],[20, 21],[3 ,2],[18, 20],[2 ,5],[5 ,3]],
	scale,"tank"))
    let min = new V3(0,0,0)
    let max = new V3(0,0,0)
    for(let vert of this.verts) {
        for(let coord of ["x","y","z"]) {
            if(vert[coord] < min[coord]) min[coord] = vert[coord]
            if(vert[coord] > max[coord]) max[coord] = vert[coord] 
        }
    }
    let aV = min.clone().add(max).multiply(0.5)
    for(let vert of this.verts) vert.sub(aV)
    max.sub(aV).multiply(scale)
    min.sub(aV).multiply(scale)
    this.pos.y = Math.abs(min.y)
    let index = meshes.length
    meshes.push(this)
    
    let t = 0
    let sequence = Date.now();
    let fired = Date.now();

    this.getBack = () => {
        let destination = (new V3(0,0,160)).rotate(new V3(0,t,0))
        let tmp = destination.clone().sub(this.pos)
        this.rot.y = Math.atan2(tmp.x,tmp.z)
        this.pos.add((new V3(0,0,0.45)).rotate(new V3(0,this.rot.y,0)))
        let target = camera.pos.clone().sub(this.pos)
        if(target.length() < 40 && camera.pos.length() < 160) sequence = Date.now(), this.worker = this.attack; 
        if(tmp.length() <= 0.46) sequence = Date.now(), this.worker = this.idle;
    }

    this.idle = () => {
        t -= 0.006
        let destination = (new V3(0,0,160)).rotate(new V3(0,t,0))
        let tmp = destination.clone().sub(this.pos)
        this.rot.y = Math.atan2(tmp.x,tmp.z)
        this.pos = destination
        this.pos.y = Math.abs(min.y)
        let target = camera.pos.clone().sub(this.pos)
        if(Date.now() - sequence > 7000 || target.length() < 80 && camera.pos.length() < 160) this.worker = this.attack, sequence = Date.now();
    }

    this.attack = () => {
        let target = camera.pos.clone().sub(this.pos)
        this.rot.y = Math.atan2(target.x,target.z)
        if(Date.now() - fired > 2000) {
            let p = new Projectile(0.5,meshes,this.rot.clone(),this.pos.clone().add(new V3(0,1.3,0)),"tank",camera);
            fired = Date.now();
        }  
        if(target.length() > 40) this.pos.add((new V3(0,0,0.45)).rotate(new V3(0,this.rot.y,0)))
        if(Date.now() - sequence > 10000 || camera.pos.length() >= 160) this.worker = this.getBack;
    }
    
    this.worker = this.idle

    

    this.destroy = () => {
        meshes[index] = undefined;
        this.worker = () => {}
    }

}