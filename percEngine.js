function P3(x,y,z) {
    return {x: x, y: y, z: z}
}

function P2(x,y) {
    return {x: x, y: y}
}

function V3(x,y,z) {
    Object.assign(this,P3(x,y,z));
    this.clone = () => {
        return new V3(this.x,this.y,this.z)
    }
    this.sub = (V) => {
        return Object.assign(this, P3(this.x-V.x,this.y-V.y,this.z-V.z))
    }
    this.add = (V) => {
        return Object.assign(this, P3(this.x+V.x,this.y+V.y,this.z+V.z))
    }
    this.length = (V) => {
        return Math.sqrt(this.x*this.x+this.z*this.z)
    }
    this.length2 = (V) => {
        return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z)
    }
    this.rotate = (V) => {
      if(V.x) {
        let C = Math.sqrt(this.y*this.y+this.z*this.z)
        let t = Math.sign(this.y)*Math.acos(this.z/C)
        this.y = C*Math.sin(t+V.x)
        this.z = C*Math.cos(t+V.x)
      }
      if(V.z) {
        let C = Math.sqrt(this.y*this.y+this.x*this.x)
        let t = Math.sign(this.y)*Math.acos(this.x/C)
        this.y = C*Math.sin(t+V.z)
        this.x = C*Math.cos(t+V.z)
      }
      if(V.y) {
        let C = Math.sqrt(this.x*this.x+this.z*this.z)
        let t = Math.sign(this.x)*Math.acos(this.z/C)
        this.x = C*Math.sin(t+V.y)
        this.z = C*Math.cos(t+V.y)
      }
      return this
    }
    this.multiply = (num) => {
        return Object.assign(this, P3(this.x*num,this.y*num,this.z*num))
    }
}

function Mesh(verts,lines,scale,name) {
    this.verts = verts;
    this.lines = lines;
    this.scale = scale;
    this.name = name;
    this.rot = new V3(0,0,0)
    this.pos = new V3(0,0,0)

}

function Camera (stats,ctx,ctxPos,ctxPos2,meshes,pos,rot) {
    this.pos = pos
    this.rot = rot
    this.stats = stats
    let w = ctxPos2.x-ctxPos.x, h = ctxPos2.y-ctxPos.y
    let invert = false
    
    this.colors = () => {
        invert = true;
        setTimeout(()=>{
            invert = false;
        },120)     
    }

    this.updateFrame = () => {
        if(stats.black) {
            ctx.fillStyle = "black"
            ctx.fillRect(ctxPos.x,ctxPos.y,w,h);
            return;
        }
        ctx.fillStyle = invert ? "white" : "rgb(79,68,216)"
        ctx.fillRect(ctxPos.x,ctxPos.y,w,h*(72/160));
        ctx.fillStyle = invert ? "white" : "rgb(127,83,7)"
        ctx.fillRect(ctxPos.x,h*(72/160)+ctxPos.y,w,h*(88/160));
        ctx.beginPath();

        let nW = (222+248)/248*w
        let range = this.rot.y-(parseInt(this.rot.y.toFixed(15)/(2*Math.PI))*(2*Math.PI))
        let t = -(range < 0 ? (2*Math.PI)-Math.abs(range) : range)/(2*Math.PI)*nW

        ctx.moveTo(w*(75/248)+ctxPos.x+t,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*(167/248)+ctxPos.x+t,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*(75/248)+ctxPos.x+t,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*(125/248)+ctxPos.x+t,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*(75/248)+ctxPos.x+t,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91)/248)+ctxPos.x+t,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-22)/248)+ctxPos.x+t,h*(38/160)+ctxPos.y)
        ctx.lineTo(w*((75-91+38)/248)+ctxPos.x+t,h*(55/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-22)/248)+ctxPos.x+t,h*(38/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-67)/248)+ctxPos.x+t,h*(57/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-22)/248)+ctxPos.x+t,h*(38/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-56)/248)+ctxPos.x+t,h*(63/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-40-75)/248)+ctxPos.x+t,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-40)/248)+ctxPos.x+t,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-40-75)/248)+ctxPos.x+t,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-40-42)/248)+ctxPos.x+t,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-40-75)/248)+ctxPos.x+t,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-40-75-91)/248)+ctxPos.x+t,h*(72/160)+ctxPos.y)

        ctx.moveTo(w*(75/248)+ctxPos.x+t+nW,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*(167/248)+ctxPos.x+t+nW,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*(75/248)+ctxPos.x+t+nW,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*(125/248)+ctxPos.x+t+nW,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*(75/248)+ctxPos.x+t+nW,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91)/248)+ctxPos.x+t+nW,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-22)/248)+ctxPos.x+t+nW,h*(38/160)+ctxPos.y)
        ctx.lineTo(w*((75-91+38)/248)+ctxPos.x+t+nW,h*(55/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-22)/248)+ctxPos.x+t+nW,h*(38/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-67)/248)+ctxPos.x+t+nW,h*(57/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-22)/248)+ctxPos.x+t+nW,h*(38/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-56)/248)+ctxPos.x+t+nW,h*(63/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-40-75)/248)+ctxPos.x+t+nW,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-40)/248)+ctxPos.x+t+nW,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-40-75)/248)+ctxPos.x+t+nW,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-40-42)/248)+ctxPos.x+t+nW,h*(72/160)+ctxPos.y)
        ctx.moveTo(w*((75-91-40-75)/248)+ctxPos.x+t+nW,h*(31/160)+ctxPos.y)
        ctx.lineTo(w*((75-91-40-75-91)/248)+ctxPos.x+t+nW,h*(72/160)+ctxPos.y)


        ctx.moveTo(ctxPos.x,h*(72/160)+ctxPos.y)
        ctx.lineTo(w+ctxPos.x,h*(72/160)+ctxPos.y)
        for(let mesh of meshes) {
                if(!mesh) continue
                let projectedVerts = []
                for(let vert of mesh.verts) {
                    let D = vert.clone().multiply(mesh.scale).rotate(mesh.rot).add(mesh.pos).sub(this.pos)
                    let X = D.x*Math.cos(this.rot.y)-D.z*Math.sin(this.rot.y)
                    let Z = D.z*Math.cos(this.rot.y)+D.x*Math.sin(this.rot.y)
                    let Y = -(D.y*Math.cos(this.rot.x)+Z*Math.sin(this.rot.x))
                    Z = Z*Math.cos(this.rot.x)+Y*Math.sin(this.rot.x)
                    let F = (w*0.6)/Z
                    if(Z > 0) projectedVerts.push([X*F+w/2+ctxPos.x, Y*F+h*(72/160)+ctxPos.y])
                    else projectedVerts.push(false)
                }
                for(let line of mesh.lines) {
                    if(!projectedVerts[line[0]] || !projectedVerts[line[1]]) continue
                    ctx.moveTo(projectedVerts[line[0]][0], projectedVerts[line[0]][1]);
                    ctx.lineTo(projectedVerts[line[1]][0], projectedVerts[line[1]][1]);
                    //ctx.fillStyle = "yellow"
                    //ctx.font = '20px serif';
                    //ctx.fillText(line[0], projectedVerts[line[0]][0], projectedVerts[line[0]][1]);
                    //ctx.fillText(line[1], projectedVerts[line[1]][0], projectedVerts[line[1]][1]);
                }
        }
        ctx.closePath();
        ctx.lineWidth = 2.5;
        ctx.strokeStyle = invert ? "rgb(147,58,76)" : "white";
        ctx.stroke();


    }
}

//87
//s72

// w 248 y 145